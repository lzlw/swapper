#!/usr/bin/env python

import argparse
import sys

"""
TO DO:
 - History of all swapped letters
 - Save output
 - Rework menu system
 - Document functions
 - Change workflow
"""

parser = argparse.ArgumentParser(description='basic substitution solver')
parser.add_argument('InputFile', help='file to read from')

def swap(x, y, data):
	new = ''
	for i in range(len(data)):
		if data[i] == x:
			new += y
		else:
			new += data[i]
			
	return new
	
def get_data(inputfile):
	try:
		f = open(inputfile, 'r')
	except:
		print '[-] Error opening file %s' % inputfile
		print '[-] Exiting...'
		sys.exit(1)
	else:
		data = f.read()
		f.close()
		
	return data.upper()
	
def display_help():
	print '--------'
	print 'commands'
	print '--------\n'
	print '[X][Y]:\tswap X with Y'
	print 'print:\tshow data'
	print 'revert:\trevert to original'
	print 'quit:\texit'
	print 'help:\tdisplay this message\n'
	
def main():
	# parse args
	args = parser.parse_args()
	
	# load file
	data = get_data(args.InputFile)
	
	# loop through user selection
	choice = ''
	while True:
		choice = raw_input('swapper % ')
		if choice == 'quit':
			sys.exit(0)
		elif choice == 'print':
			print data
		elif choice == 'revert':
			data = get_data(args.InputFile)
		elif choice == 'help':
			display_help()
		elif len(choice) == 2:
			data = swap(choice[0], choice[1], data)
		else:
			print 'unrecognized command: %s' % choice

if __name__ == '__main__':
	main()
